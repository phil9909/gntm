#!/usr/bin/env bash

IMAGE=phil9909/drinkinggames
TAG=v0.0.5

docker build --tag $IMAGE:$TAG .
docker push $IMAGE:$TAG
