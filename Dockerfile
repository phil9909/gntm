FROM phil9909/hugo:0.63.1 AS hugo
WORKDIR /var/build/
ADD . ./
RUN ["hugo"]

FROM busybox
USER 1000:1000
WORKDIR /var/www
COPY --from=hugo /var/build/public /var/www
ENTRYPOINT ["httpd", "-f", "-p", "8080"]
