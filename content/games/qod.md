---
title: QoD&trade; the Drinking Game
description: Das Trinkspiel, das Queen of Drags&trade; erträglich macht.

subdomain: qod
theme:
  name: "drag"
  color: "#7B1FA2"
copyright:
  - subjects: ["Queen of Drags"]
    holder: "ProSiebenSat.1 TV Deutschland GmbH"
---

# QoD&trade; the Drinking Game

## Die Regeln

In folgenden Fällen ist **ein Schluck** zu nehmen:

- Jemand sagt "**Drags**"
- Jemand sagt "**Foto**"
- Heidi sagt: "**super**"
- Heidi ist überbelichtet
- Jemand **heult**
- Jemand verwendet einen unnötigen Anglizismus (meist "**Shooting**", "**Competition**" oder "**Attitude**")
- Eine Bitch lästert über eine andere Bitch in die Kamera

In folgenden Fällen ist **exen** angesagt:

- Heidi sagt: "**Nur eine(r) kann Queen of Drags werden**"

{{< theme-switch options="drag:#7B1FA2:Drag blue:#303F9F:Blau green:#689F38:Grün" >}}
