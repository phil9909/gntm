---
title: GNTM&trade; the Drinking Game
description: Das Trinkspiel, das Germany's next Topmodel&trade; erträglich macht.

subdomain: gntm
theme:
  name: "topmodel"
  color: "#C2185B"
copyright:
  - subjects: ["GNTM", "Germany's next Topmodel"]
    holder: "ProSiebenSat.1 TV Deutschland GmbH"
---

# GNTM&trade; the Drinking Game

## Die Regeln

In folgenden Fällen ist **ein Schluck** zu nehmen:

- Jemand sagt "**Mädchen**"
- Jemand sagt "**Foto**"
- Heidi sagt: "**super**"
- Jemad **heult**
- Jemand verwendet einen unnötigen Anglizismus (meist "**Shooting**", "**Competition**" oder "**Attitude**")
- Wird aus gutem Grund Englisch gesprochen (zum Beispiel weil ein Gastjuror kein Deutsch versteht), ist auf die Untertitel zu achten. Auch dort verstecken sich Anglizismen.
- Eine Bitch lästert über eine andere Bitch in die Kamera
- Heidi vertont ein Küsschen "**muah**"

In folgenden Fällen ist **exen** angesagt:

- Heidi sagt: "**Nur eine kann Germany's next Topmodel werden**"
- Heidi sagt: "**Nur einer kann Germany's next Topmodel werden**"
- Andere Variationen wie "eine oder einer kann [...]"

{{< theme-switch options="topmodel:#C2185B:Topmodel blue:#303F9F:Blau green:#689F38:Grün" >}}
