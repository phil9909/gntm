---
title: New Kids&trade; the Drinking Game
description: Das Trinkspiel, das New Kids&trade; noch besser macht.

subdomain: newkids
theme:
  name: "newkids"
  color: "#7B1FA2"
copyright:
  - subjects: ["New Kids Turbo", "New Kids Nitro"]
    holder: "Warner Bros Entertainment Europe Co."
---

# New Kids the Drinking Game

## Die Regeln

### Normale Version

In folgenden Fällen ist **ein Schluck** zu nehmen:

- Jemand sagt "**Junge**"

### Harte Version

In folgenden Fällen ist **ein Schluck** zu nehmen:

- Jemand sagt "**Junge**"
- Jemand sagt "**Maaskantje**"
- Wenn der grüne Opel Manta zu sehen ist

In folgenden Fällen ist **exen** angesagt oder **ein Shot** zu nehmen:

- Jemand sagt: "**Verrückte Mongo**"

{{< theme-switch options="newkids:#7B1FA2:New%20Kids blue:#303F9F:Blau" >}}
