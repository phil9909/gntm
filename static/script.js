let styleNames;

function loadListener() {
  if (window.CSS && window.CSS.supports && window.CSS.supports('(--variable: #000000)')) {
    styleDivs = document.querySelectorAll("div.styleoption");
    styleNames = [];
    for(let i = 0; i < styleDivs.length; ++i) {
      let name = styleDivs[i].id.replace("styleoption-","")
      styleNames.push(name);
      addListener(name);
    }
  }
  else {
    var element = document.getElementById("style");
    element.parentNode.removeChild(element);
  }
}

function clickListenerFactory(name) {
  return function(event) {
    setColorset(name, event.currentTarget.getAttribute("x-color"));
  };
};

function addListener( name ) {
  document.getElementById("styleoption-" + name).addEventListener("click", clickListenerFactory(name));
}

function setColorset( name, themecolor ) {
  for(var i = 0; i < styleNames.length; ++i) {
    var element = document.getElementById("styleoption-" + styleNames[i]);
    element.classList.remove("selected");
  }
  document.getElementById("palette").setAttribute("href", "/palette_" + name + ".css");
  
  var element = document.getElementById("styleoption-" + name);
  element.classList.add("selected");
  
  document.getElementById("themecolor").setAttribute("content", themecolor);
}


window.addEventListener("load", loadListener);
